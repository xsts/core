/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.about;

import org.xsts.core.terminal.TextLine;
import org.xsts.core.terminal.TextWindow;

public class AboutCore {

    public static final String PROJECT_NAME = "Core";
    public static final String VERSION = "0.2.11";

    public static void main(String ... args) {
        TextWindow tw = new TextWindow(80, 3);
        tw.addTextLine(new TextLine(projectName()));
        tw.addTextLine(new TextLine(currentVersion()));
        tw.addTextLine(new TextLine(memberOf()));
        tw.addTextLine(new TextLine(organizationName()));
        tw.addTextLine(new TextLine(sinceYear()));
        tw.print();
    }

    static  String organizationName(){
        return new StringBuilder()
                .append("Author: ")
                .append(InfoTransPub.FULL_ORGANIZATION_NAME)
                .toString();
    }

    static  String memberOf() {
        return new StringBuilder()
                .append("Framework: ")
                .append(InfoTransPub.SHORT_GROUP_NAME)
                .toString();
    }

    static  String projectName() {
        return new StringBuilder()
                .append("Module: ")
                .append(PROJECT_NAME)
                .toString();
    }

    static  String sinceYear() {
        return new StringBuilder()
                .append("Since: ")
                .append("2016")
                .toString();
    }

    static  String currentVersion() {
        return new StringBuilder()
                .append("Version: ")
                .append(VERSION)
                .toString();
    }

}
