package org.xsts.core.about;

/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */

public class InfoTransPub {
    public static final String FULL_ORGANIZATION_NAME = "The Open Initiative for Information about Public Transportation";
    public static final String SHORT_ORGANIZATION_NAME = "infotranspub";
    public static final String FOUNDATION_YEAR = "2016";
    public static final String SHORT_GROUP_NAME = "XSTS";
    public static final String PROJECT_CREATED_BY = "Created and maintained by";

}
