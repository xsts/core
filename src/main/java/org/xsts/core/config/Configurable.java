/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.config;

public interface Configurable {
     Boolean contains(String key);
     Object get(String propertyName);
     String getString(String propertyName);
     Integer getInteger(String propertyName);
     Double getDouble(String propertyName);
     Configurable put(String propertyName, Object propertyValue);
     String variable(String var);
}
