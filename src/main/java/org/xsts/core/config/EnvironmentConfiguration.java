/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.config;

import org.xsts.core.config.param.Parameter;
import org.xsts.core.config.update.Updatable;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class EnvironmentConfiguration implements Configurable {

    private static EnvironmentConfiguration instance = null;
    private Map<String, Parameter> properties = null;

    private EnvironmentConfiguration(){
        properties = new HashMap<>();
    }

    public static EnvironmentConfiguration getInstance() {
        if (instance == null ) {
            synchronized (EnvironmentConfiguration.class){
                if (instance == null ) {
                    instance = new EnvironmentConfiguration();
                }
            }
        }
        return instance;
    }
    @Override
    public EnvironmentConfiguration put(String propertyName, Object propertyValue) {

        if (propertyValue instanceof String) {
            String value = (String) propertyValue;
            String replacer = value;
            for (String property : properties.keySet()) {
                if (value.indexOf(property) > -1) {
                    Parameter parameterAlias = properties.get(property);
                    String subst = parameterAlias.getStringValue();
                    Parameter parameter = properties.get(subst);
                    String realSubst = parameter.getStringValue();
                    replacer = value.replaceAll(Pattern.quote(property), realSubst);
                    value = replacer;
                }
            }
            properties.put(propertyName, new Parameter(propertyName,value  ));
        } else {
            properties.put(propertyName, new Parameter(propertyName,propertyValue  ));
        }
        return this;
    }

    @Override
    public Boolean contains(String key) {
        return  properties.containsKey(key);
    }

    @Override
    public Object get(String propertyName){
        return properties.get(propertyName).getObjectValue();
    }

    @Override
    public String getString(String propertyName){
        return properties.get(propertyName).getStringValue();
    }

    @Override
    public Integer getInteger(String propertyName){
        return properties.get(propertyName).getIntegerValue();
    }

    @Override
    public Double getDouble(String propertyName){
        return properties.get(propertyName).getDoubleValue();
    }

    @Override
    public  String variable(String var) {
        StringBuilder sb = new StringBuilder();
        sb.append("${").append(var).append("}");
        return sb.toString();
    }


    public EnvironmentConfiguration source(Updatable updatable){
        updatable.update(this);
        return this;
    }
}
