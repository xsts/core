/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.config.param;

import java.awt.Color;

public class Parameter {
    private String name = null;
    private Object value = null;
    private ParameterType type;

    public Parameter(String aName, Object aValue) {
        this.name = aName;
        this.value = aValue;

        if (aValue instanceof String)
            this.type = ParameterType.STRING_TYPE;
        else if (aValue instanceof Integer)
            this.type = ParameterType.INTEGER_TYPE;
        else if (aValue instanceof Double)
            this.type = ParameterType.DOUBLE_TYPE;
        else if (aValue instanceof Color)
            this.type = ParameterType.COLOR_TYPE;
        else
            this.type = ParameterType.OBJECT_TYPE;
    }

    public String getName() { return name;}
    public Object getObjectValue() { return value;}
    public ParameterType getType() { return type;}

    public String getStringValue() {
        if (type == ParameterType.STRING_TYPE)
            return (String)value;
        return null;
    }

    public Integer getIntegerValue() {
        if (type == ParameterType.INTEGER_TYPE)
            return (Integer)value;
        return null;
    }

    public Double getDoubleValue() {
        if (type == ParameterType.DOUBLE_TYPE)
            return (Double) value;
        return null;
    }

    public Color getColorValue() {
        if (type == ParameterType.COLOR_TYPE)
            return (Color)value;
        return null;
    }
}