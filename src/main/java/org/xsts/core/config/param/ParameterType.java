/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.config.param;

public  enum ParameterType {
    OBJECT_TYPE,
    STRING_TYPE,
    INTEGER_TYPE,
    DOUBLE_TYPE,
    COLOR_TYPE
}
