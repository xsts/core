/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.config.update;

import org.xsts.core.config.Configurable;

public class FileSystemUpdater implements Updatable {
    public static final String ENV_ROOT_DIR = "ROOT_DIR";
    public static final String ENV_DATA_INPUT_DIR = "DATA_INPUT_DIR";
    public static final String ENV_DATA_OUTPUT_DIR = "DATA_OUTPUT_DIR";
    public static final String ENV_AGENCY = "AGENCY";
    public static final String ENV_ROUTE_COLOR_DIR = "ROUTE_COLOR_DIR";
    public static final String ENV_AGENCY_CATALOG_DIR = "AGENCY_CATALOG_DIR";
    public static final String ENV_GTFS_INPUT_PATH = "GTFS_INPUT_PATH";

    public static final String ROOT_DIR = "fsc.root_directory";
    public static final String DATA_INPUT_DIR = "fsc.data_source_directory";
    public static final String DATA_OUTPUT_DIR = "fsc.data_destination_directory";
    public static final String AGENCY = "fsc.agency";
    public static final String ROUTE_COLOR_DIR = "fsc.route_color_directory";
    public static final String AGENCY_CATALOG_DIR = "fsc.agency_catalog_directory";
    public static final String GTFS_INPUT_PATH = "fsc.data_gtfs_input_path";
    public static final String GTFS_OUTPUT_PATH = "fsc.data_gtfs_output_path";

    @Override
    public void update(Configurable configurable) {
        configurable.put(configurable.variable(ENV_ROOT_DIR),ROOT_DIR);
        configurable.put(configurable.variable(ENV_DATA_INPUT_DIR),DATA_INPUT_DIR);
        configurable.put(configurable.variable(ENV_DATA_OUTPUT_DIR),DATA_OUTPUT_DIR);
        configurable.put(configurable.variable(ENV_AGENCY),AGENCY);
        configurable.put(configurable.variable(ENV_ROUTE_COLOR_DIR),ROUTE_COLOR_DIR);
        configurable.put(configurable.variable(ENV_AGENCY_CATALOG_DIR),AGENCY_CATALOG_DIR);
        configurable.put(configurable.variable(ENV_GTFS_INPUT_PATH),GTFS_INPUT_PATH);
    }
}
