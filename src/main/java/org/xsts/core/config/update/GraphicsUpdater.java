/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.config.update;

import org.xsts.core.config.Configurable;

public class GraphicsUpdater implements Updatable{
    public static final String FOREGROUND_COLOR = "gc.foreground_color";
    public static final String BACKGROUND_COLOR = "gc.background_color";

    @Override
    public void update(Configurable config) {
    }
}
