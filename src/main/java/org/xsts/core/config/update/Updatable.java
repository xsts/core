/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.config.update;

import org.xsts.core.config.Configurable;

public interface Updatable {
     void update(Configurable config);
}
