/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.data.collections;

public interface KeyValueList {
    void add(String id, Object object);
    Object get(String id);
    boolean isEmpty();
}
