/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.data.collections;

public interface LinearValueList {
    void add( Object object);
    Object getAt(int pos);
    boolean isEmpty();
}
