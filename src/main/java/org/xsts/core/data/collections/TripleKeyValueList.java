/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.data.collections;

public interface TripleKeyValueList {
    void add(String key1, String key2, String key3, Object object);
    Object get(String key1);
    Object get(String key1, String key2);
    Object get(Object key1, String key2, String key3);
    boolean isEmpty();
}
