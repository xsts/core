/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.data.util;

public class Haversine {
    public static double  getDistanceCoordLong(long  lat1, long lon1 , long lat2, long lon2){
        double dLat1 = asDouble(lat1);
        double dLat2 = asDouble(lat2);
        double dLon1 = asDouble(lon1);
        double dLon2 = asDouble(lon2);
        return getDistanceCoordDouble(dLat1, dLon1, dLat2, dLon2);
    }

    public static double getDistanceCoordDouble(double  lat1, double lon1 , double lat2, double lon2 ){
        double latf;
        double latt;
        double lonf;
        double lont;
        double dist = 0;

        latf = toRadians(lat1);
        lonf = toRadians(lon1);
        latt = toRadians(lat2);
        lont = toRadians(lon2);
        dist = distanceHaversine(latf, lonf, latt, lont, 0.0000001);
        return dist;
    }

    public static double asDouble(long value) {
        double v1 = (double) value;
        double f1 = 1000000.0;
        return v1 / f1;
    }

    public static double inverseHaversine(double d){
        return 2 * Math.atan2(Math.sqrt(d), Math.sqrt(1 - d)) * 6378137.0;
    }

    public static double distanceHaversine(double latf, double lonf, double latt, double lont,
                                           double tolerance){
        double d;
        double dlat = latt - latf;
        double dlon =  lont - lonf;

        d = (Math.sin(dlat * 0.5) * Math.sin(dlat * 0.5)) + (Math.cos(latf) * Math.cos(latt)) * (Math.sin(dlon * 0.5) * Math.sin(dlon * 0.5));
        if(d > 1 && d <= tolerance){
            d = 1;
        }
        return inverseHaversine(d);
    }

    public static double toRadians(double deg){
        return deg * 0.01745329251;  // PI / 180;
    }
}
