/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.data.util;

import java.util.regex.Pattern;

public class LatLongConvertor {
    public static final int DECIMAL_FACTOR = 10;
    public static final int NO_DECIMAL = 1;
    public static final int ONE_DECIMAL = NO_DECIMAL * DECIMAL_FACTOR;
    public static final int TWO_DECIMAL = ONE_DECIMAL * DECIMAL_FACTOR;
    public static final int THREE_DECIMAL = TWO_DECIMAL * DECIMAL_FACTOR;
    public static final int FOUR_DECIMAL = THREE_DECIMAL * DECIMAL_FACTOR;
    public static final int FIVE_DECIMAL = FOUR_DECIMAL * DECIMAL_FACTOR;
    public static final int SIX_DECIMAL = FIVE_DECIMAL * DECIMAL_FACTOR;
    public static final int MASK = SIX_DECIMAL;


    private static final int[] CONVERT_LENGTHS = {NO_DECIMAL,ONE_DECIMAL, TWO_DECIMAL, THREE_DECIMAL,
            FOUR_DECIMAL, FIVE_DECIMAL, SIX_DECIMAL };


    public static final int SUB_UNIT_DELTA = 1;
    public static final int NO_SUB_UNIT = 0;
    public static final int ONE_SUB_UNIT = NO_SUB_UNIT + SUB_UNIT_DELTA;
    public static final int TWO_SUB_UNIT = ONE_SUB_UNIT + SUB_UNIT_DELTA;
    public static final int THREEE_SUB_UNIT = TWO_SUB_UNIT + SUB_UNIT_DELTA;
    public static final int FOUR_SUB_UNIT = THREEE_SUB_UNIT + SUB_UNIT_DELTA;
    public static final int FIVE_SUB_UNIT = FOUR_SUB_UNIT + SUB_UNIT_DELTA;
    public static final int SIX_SUB_UNIT = FIVE_SUB_UNIT + SUB_UNIT_DELTA;
    public static final int MASK_UNIT = SIX_SUB_UNIT;




    public static long convertSDL(String coordinate){
        Double doubleCoord = Double.parseDouble(coordinate);
        doubleCoord *= MASK;
        long longCoord = (long)(double)doubleCoord;
        return longCoord;
    }

    public static long convertSPL(String coordinate, int precision) {
        String [] tokens = coordinate.split(Pattern.quote("."));
        Long coord = Long.parseLong(tokens[0]);
        StringBuilder sb = new StringBuilder();

        if ( tokens[1].length() <= precision){
            sb.append(tokens[1]);
            for ( int i = precision - tokens[1].length(); i > 0; i--)
                sb.append("0");
            for ( int i = MASK_UNIT - precision; i > 0; i++)
                sb.append("0");
        } else {
            sb.append(tokens[1].substring(0,precision));
        }
        Long units = Long.parseLong(sb.toString());

        coord *= MASK;
        coord += units;

        return coord;
    }
}
