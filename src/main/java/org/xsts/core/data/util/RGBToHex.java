/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.data.util;

import java.awt.Color;

public class RGBToHex {
    public static String convert(Color color){
        String result = convert(color.getRed()) + convert(color.getGreen()) +convert(color.getBlue());
        return result;
    }
    public static String convert(int channelColor){
        int leftValue = channelColor / 16;
        int rightValue = channelColor % 16;
        return convertToHex(leftValue) + convertToHex(rightValue);
    }
    public static String convertToHex(int value){
        switch (value) {
            case 1: return "1";
            case 2: return "2";
            case 3: return "3";
            case 4: return "4";
            case 5: return "5";
            case 6: return "6";
            case 7: return "7";
            case 8: return "8";
            case 9: return "9";
            case 10: return "A";
            case 11: return "B";
            case 12: return "C";
            case 13: return "D";
            case 14: return "E";
            case 15: return "F";
            case 0:
            default:
                return "0";
        }
    }
}
