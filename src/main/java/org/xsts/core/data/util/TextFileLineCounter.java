/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.data.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TextFileLineCounter {
    public static int countLines(Path sourcePath,
                                 String fileName) {
        Path thePath = Paths.get(sourcePath.toAbsolutePath().toString(), fileName);
        int count = 0;
        int pos = 0;
        try (BufferedReader reader
                     = new BufferedReader(new FileReader(thePath.toString()))) {
            while (reader.readLine() != null) {
                pos++;
            }
            reader.close();

            count = pos;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }


    public static int countLines(Path sourcePath) {
        Path thePath = sourcePath;
        int count = 0;
        int pos = 0;
        try (BufferedReader reader
                     = new BufferedReader(new FileReader(thePath.toString()))) {
            while (reader.readLine() != null) {
                pos++;
            }
            reader.close();

            count = pos;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }
}
