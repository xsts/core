/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.terminal;

public class TextLine {
    private final String text;

    public TextLine(String text) {
        this.text = text;
    }

    public String text() { return this.text;}
}
