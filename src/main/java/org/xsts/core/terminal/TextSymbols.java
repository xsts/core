/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.terminal;

public class TextSymbols {
    public static final Character SPACE = ' ';
    public static final Character CORNER  = '+';
    public static final Character HLINE  = '-';
    public static final Character VLINE  = '|';


}
