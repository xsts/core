/*
 * Group : XSTS
 * Project : Core
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.core.terminal;

import java.util.ArrayList;
import java.util.List;

public class TextWindow {
    private int cols;
    private int lines;
    Character[][] buffer;
    List<TextLine> content = null;

    public TextWindow(int cols, int printableLines) {
        this.cols = cols;
        this.lines = printableLines + 2;
        buffer = new Character[lines][cols];
        initWindow();

        content = new ArrayList<>();
    }

    void adjustVerticalSize(int printableLines) {
        this.lines = printableLines + 2;
        buffer = new Character[lines][cols];
        initWindow();
    }

    void initWindow() {
        emptyBuffer();
    }

    void emptyBuffer() {
        for ( int l = 0; l < lines; l++){
            for (int c = 0; c < cols; c++){
                buffer[l][c] = TextSymbols.SPACE;
            }
        }
    }

    public void addTextLine(TextLine textLine){
        content.add(textLine);
    }

    public void print() {
        adjustVerticalSize(content.size());
        refreshBuffer();
        printBuffer();
    }

    void printBuffer(){

    }

    void refreshBuffer() {
        emptyBuffer();
        syncBuffer();
        flushBuffer();
    }

    private void flushBuffer() {
        for ( int l = 0; l < lines; l++) {
            StringBuilder sb = new StringBuilder();
            for ( int c = 0; c < cols; c++){
                sb.append(buffer[l][c]);
            }
            System.out.println(sb.toString());
        }
    }

    private void syncBuffer() {
        buffer[0][0] = TextSymbols.CORNER;
        buffer[0][cols-1] = TextSymbols.CORNER;
        buffer[lines-1][0] = TextSymbols.CORNER;
        buffer[lines-1][cols-1] = TextSymbols.CORNER;

        for ( int l = 1; l < lines -1; l++){
            buffer[l][0] = TextSymbols.VLINE;
            buffer[l][cols-1] = TextSymbols.VLINE;
        }

        for ( int c = 1; c < cols -1; c++){
            buffer[0][c] = TextSymbols.HLINE;
            buffer[lines -1][c] = TextSymbols.HLINE;
        }

        int bestVerticalSize = content.size();
        if ( bestVerticalSize > lines -2)
            bestVerticalSize = lines -2;

        int bestHorizontalSize = 0;
        for ( TextLine tl: content){
            if ( tl.text().length() > bestHorizontalSize)
                bestHorizontalSize = tl.text().length();
        }

        if ( bestHorizontalSize > cols -4)
            bestHorizontalSize = cols -4;

        int vpos = 1;
        for ( TextLine tl: content){
            String text = tl.text();
            for ( int pos = 0; pos <bestHorizontalSize && pos < text.length(); pos++ ){
                int hpos = 2 + pos;
                buffer[vpos][hpos] = text.charAt(pos);
            }

            vpos ++;
        }

    }



}
